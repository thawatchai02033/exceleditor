import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import JsonExcel from 'vue-json-excel'

Vue.component('downloadExcel', JsonExcel)
Vue.config.productionTip = false

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
